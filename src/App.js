import { useReducer } from 'react'

import './App.css'

import { Context } from './context'
import { reducer } from './reducer'

import Counter from './components/Counter'
import Sockets from './components/Sockets'

function App() {
  const [state, dispatch] = useReducer(reducer, { count: 0 })

  return (
    <Context.Provider value={state}>
      <div className="container">
        <button onClick={() => dispatch({ type: 'UP' })}>
          Я прибавлю
        </button>
        <button onClick={() => dispatch({ type: 'DOWN' })}>
          Я отниму
        </button>
        <button onClick={() => dispatch({ type: 'RESET' })}>
          Обнуляем
        </button>
        <Counter />
        <Sockets />
      </div>
    </Context.Provider>
  )
}

export default App
