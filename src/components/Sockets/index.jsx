import { useEffect, useRef, useState } from 'react'

const Sockets = () => {
  const [status, setStatus] = useState('')
  const [msg, setMsg] = useState([])
  const [clients, setClients] = useState([])
  const ws = useRef(null)
  const input = useRef(null)

  useEffect(() => {
    const listener = (event) => {
      if (event.keyCode === 13 && input.current.value !== '') {
        ws.current.send(input.current.value)
        input.current.value = ''
      }
    }

    ws.current = new WebSocket('ws://localhost:8080/')

    ws.current.onopen = () => {
      setStatus('Лет го чат')
    }

    ws.current.onmessage = (e) => {
      const data = JSON.parse(e.data)
      setMsg((prev) => [...prev, data])
      setClients((prev) => [...prev, data.id])
    }

    document.addEventListener('keydown', listener)

    return () => {
      document.removeEventListener('keydown', listener)
      ws.current.close()
    }
  }, [])

  return (
    <div style={{ marginTop: '100px' }}>
      <p>{status}</p>
      <input type="text" ref={input} />
      <button
        onClick={() => {
          ws.current.send(input.current.value)
          input.current.value = ''
        }}
      >
        отправить
      </button>
      {msg.map((item, i) => {
        return (
          <p
            key={i}
            style={{
              color: item.id === clients[0] ? 'red' : 'green',
            }}
          >
            {item.message}
          </p>
        )
      })}
    </div>
  )
}

export default Sockets
