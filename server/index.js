const http = require('http');
const WebSocket  = require("ws");

const PORT = 8080;

const server = http.createServer();
const wss = new WebSocket.Server({server})

server.listen(PORT, () => {
  console.log(`WebSocket server is running on PORT ${PORT}`);
});


wss.on('connection', function(ws) {
  ws.id = Math.random();

  ws.on('message', function(message) {
    const data = {
      message,
      id: ws.id
    }

    wss.clients.forEach(client => client.send(JSON.stringify(data)))

  });
})
