export const reducer = (state, action) => {
  switch (action.type) {
    case 'UP':
      return {
        count: state.count + 1,
      }
    case 'DOWN':
      return {
        count: state.count - 1,
      }
    case 'RESET':
      return {
        count: 0,
      }
    default:
      return
  }
}
