import { useContext } from 'react'
import { Context } from '../../context'

const Counter = () => {
  const { count } = useContext(Context)

  return <div className="counter">{count}</div>
}

export default Counter
